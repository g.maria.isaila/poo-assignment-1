#include "house.h"
#include <cmath>
#include <algorithm>
#include <iostream>
#include <iomanip>

House::House(int new_size, char new_border, char new_fill) : border(new_border), fill(new_fill) {
    size = std::max(3, std::min(30, new_size));
}

int House::GetSize() const {
    return size;
}

void House::SetBorder(char newBorder) {
    if (newBorder >= '!' && newBorder <= '~')
        border = newBorder;
    else
        border = 'X';
}

void House::SetFill(char newFill) {
    if (newFill >= '!' && newFill <= '~')
        fill = newFill;
    else
        fill = '*';
}

void House::Draw() const {
    if (size < 3) return;
    int triangle_side = size + 2;
    for (int i = 1; i <= triangle_side; i++) {
        for (int x = 0; x < triangle_side - i; x++) {
            std::cout << " ";
        }
        for (int j = 0; j < i; j++) {
            std::cout << fill << " ";
        }
        std::cout << "\n";
    }
    for (int i = 0; i < size; i++) {
        std::cout << "  ";
        for (int j = 0; j < size; j++) {
            std::cout << border << " ";
        }
        std::cout << "\n";
    }
}

void House::Grow() {
    if (size < 30) size++;
}

void House::Shrink() {
    if (size > 3) size--;
}

int House::Perimeter() const {
    return (size + 2) * 3 + 4 * size;
}

double House::Area() const {
    double triangleArea = (sqrt(3) / 4) * std::pow(size + 2, 2);
    double squareArea = size * size;
    return triangleArea + squareArea;
}

void House::Summary() const {
    std::cout << "House size: " << size << " units.\n";
    std::cout << "Perimeter: " << Perimeter() << " units.\n";
    std::cout << std::fixed << std::setprecision(2);
    std::cout << "Area: " << Area() << " square units.\n";
    Draw();
}

using namespace std;

int main()
{
    // create some Houses
    House h1(-5), h2(7, '^'), h3(12, 'W', 'o'), h4(50, '$', '-');

    // display original Houses
    cout << "h1 has size = " << h1.GetSize() << " units.\n";
    h1.Draw();
    cout << "\nh2 has size = " << h2.GetSize() << " units.\n";
    h2.Draw();
    cout << "\nh3 has size = " << h3.GetSize() << " units.\n";
    h3.Draw();
    cout << "\nh4 has size = " << h4.GetSize() << " units.\n";
    h4.Draw();
    cout << '\n';

    h1.Shrink(); // demonstrate shrink
    h2.Shrink();
    h3.Grow(); // and grow
    h4.Grow();
    cout << "h1 now has size = " << h1.GetSize() << " units.\n";
    cout << "h2 now has size = " << h2.GetSize() << " units.\n";
    cout << "h3 now has size = " << h3.GetSize() << " units.\n";
    cout << "h4 now has size = " << h4.GetSize() << " units.\n";

    // demonstrate perimeter
    cout << "h2 has perimeter = " << h2.Perimeter() << " units.\n";
    cout << "h3 has perimeter = " << h3.Perimeter() << " units.\n";
    // and area
    cout << "h2 has area = " << h2.Area() << " square units.\n\n";
    cout << "h3 has area = " << h3.Area() << " square units.\n\n";

    h1.Draw();
    h1.Grow(); // show that fill character
    cout << "h1 grows:\n"; // appears only when size
    h1.Draw(); // is at least 3
    h1.Grow();
    cout << "... and grows:\n";
    h1.Draw();
    cout << '\n';

    h1 = h2; // demonstrate the default overload of the
    // assignment operator
    cout << "h1 now has size = " << h1.GetSize() << " units.\n";
    h1.Draw();

    // demonstrate the changing of border and fill characters
    h2.SetBorder('@');
    h2.SetFill('-');
    cout << "h2 now looks like:\n";
    h2.Draw();
    cout << '\n';
    h2.SetBorder('\n'); // illegal border
    h2.SetFill('\a'); // illegal fill
    cout << "h2 now looks like:\n";
    h2.Draw();
    cout << '\n';

    cout << "\nHere is a summary on h3:\n"; // demonstrate summary
    h3.Summary();

    return 0;
}

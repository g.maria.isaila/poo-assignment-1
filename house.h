#ifndef HOUSE_H
#define HOUSE_H

#include <iostream>

class House {
private:
    int size;
    char border;
    char fill;

public:
    House(int initialSize, char borderChar = 'X', char fillChar = '*');
    int GetSize() const;
    void SetBorder(char newBorder);
    void SetFill(char newFill);
    void Draw() const;
    void Grow();
    void Shrink();
    int Perimeter() const;
    double Area() const;
    void Summary() const;
};

#endif // HOUSE_H
